package edu.uchicago.gerber.labapi;

import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import edu.uchicago.gerber.labapi.jigs.search.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public static String BASE_URL = "https://api.themoviedb.org/3/search/movie?api_key=";
    public static String API_KEY = "78716d83195aed1f9c33db217b5f66d4";

    public MainActivityFragment() {
    }

    public static MainActivityFragment getInstance(){
        return new MainActivityFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View mainView;
        mainView = inflater.inflate(R.layout.fragment_main, container, false);


        FloatingActionButton fab = (FloatingActionButton) mainView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get the data from edit text
                //go fetch the search response and pass the result to to the
                EditText editText = mainView.findViewById(R.id.linlayout).findViewById(R.id.editText);

                String[] params = editText.getText().toString().split(" ");

                JSONObject jsonObject = null;
                Response response = null;

                StringBuilder url = new StringBuilder();
                url.append(BASE_URL);
                url.append(API_KEY);
                url.append("&query=");

                if (params.length == 0)
                    return;

                if (params.length == 1) {
                    url.append(params[0]);
                }
                else {
                    for(int nC = 0; nC < params.length -1; nC++){
                        url.append(params[nC] + "+");
                    }
                    url.append(params[params.length -1]);
                }

                new FetchTask().execute(url.toString());




            }
        });

        return mainView;

    }



    private class  FetchTask extends AsyncTask<String, Void, Response>{

        @Override
        protected Response doInBackground(String... strings) {
            StringBuilder stringBuilder = Util.getStringBuilder(strings[0]);
            Gson gson =  new Gson();
            return    gson.fromJson(stringBuilder.toString(), Response.class);


        }


        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);

            if (null != response) {
                ((MainActivity) getActivity()).swapInFragment(ResultFragment.newInstance(1, response), R.id.container);
            }
             else {
                ((MainActivity) getActivity()).swapInFragment(MainActivityFragment.getInstance(), R.id.container);
            }



        }
    }


}
