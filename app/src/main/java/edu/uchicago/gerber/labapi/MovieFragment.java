package edu.uchicago.gerber.labapi;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import edu.uchicago.gerber.labapi.jigs.forid.Response;
import edu.uchicago.gerber.labapi.jigs.search.Result;



public class MovieFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    public static final  String BASE = "https://api.themoviedb.org/3/movie/";
    public static final  String NEXT = "?api_key=78716d83195aed1f9c33db217b5f66d4&append_to_response=videos";


    // TODO: Rename and change types of parameters
    private Result result;


    public MovieFragment() {
        // Required empty public constructor
    }



    public static MovieFragment newInstance(Result result) {
        MovieFragment fragment = new MovieFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, result);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.result = (Result) getArguments().getSerializable(ARG_PARAM1);

        }

        //new strinbbuidler here
        https://api.themoviedb.org/3/movie/384680?api_key=78716d83195aed1f9c33db217b5f66d4&append_to_response=videos

        new FetchMovieItemTask().execute(BASE + result.getId() + NEXT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



    private class FetchMovieItemTask extends AsyncTask<String, Void, Response>{

        @Override
        protected Response doInBackground(String... strings) {
         StringBuilder stringBuilder =  Util.getStringBuilder(strings[0]);
            Gson gson =  new Gson();
            return    gson.fromJson(stringBuilder.toString(), edu.uchicago.gerber.labapi.jigs.forid.Response.class);

        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            String yahooTrailerAddress = "https://www.youtube.com/watch?v=";

            String strKey = "";
            try {
                strKey =  response.getVideos().getResults().get(0).getKey();
            } catch (Exception e) {
                Toast.makeText(getActivity(), "No youtube video for this movie", Toast.LENGTH_LONG).show();
                ((MainActivity)  getActivity()).swapInFragment(MainActivityFragment.getInstance(), R.id.container);

                return;
            }


            if (!strKey.equals("")){
                yahooTrailerAddress += strKey;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(yahooTrailerAddress));
                startActivity(browserIntent);
            } else {
                Toast.makeText(getActivity(), "No youtube video for this movie", Toast.LENGTH_LONG).show();
                ((MainActivity)  getActivity()).swapInFragment(MainActivityFragment.getInstance(), R.id.container);
            }

           // Log.d("UUTT", yahooTrailerAddress);

            //
            //Intent intent = new Intent()





        }
    }


}
