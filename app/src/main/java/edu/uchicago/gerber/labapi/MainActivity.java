package edu.uchicago.gerber.labapi;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import edu.uchicago.gerber.labapi.jigs.search.Result;

public class MainActivity extends AppCompatActivity implements

ResultFragment.OnListFragmentInteractionListener


{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }

    @Override
    protected void onResume() {
        super.onResume();

        swapInFragment(MainActivityFragment.getInstance(), R.id.container);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void swapInFragment(Fragment fragment, int containerId){
        FragmentTransaction t = getSupportFragmentManager()
                .beginTransaction();

        t.replace(containerId, fragment);
        t.commit();
    }

    @Override
    public void onListFragmentInteraction(Result item) {

        //get the result. Move fragment.
        MovieFragment movieFragment = MovieFragment.newInstance(item);
        swapInFragment(movieFragment, R.id.container);

    }
}
